-- По каждому дню последних 2-х недель считаем количество вернувшихся пользователей: 
-- Находим минимальную дату появления пользователя, и считаем ее как дату регистрации, находим всех, кто удовлетворяет условиям: зарегистрировался раньше 2-х недель назад и зашел в течение 2-х недель

select date(event_timestamp), count(user_id) as Number_of_returning_users
from (
select t1.user_id, 
t1.event_timestamp  
from table_1 t1
join ( 
	select user_id, min(date(event_timestamp)) as dt_registr 
    from table_1 
    GROUP BY 1
) as a on t1.user_id = a.user_id
and dt_registr< date(now())- interval '14' day
where t1.event_timestamp > date(now())- interval '14' day) as d
group by 1
;
