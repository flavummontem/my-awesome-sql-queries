-- По каждому месяцу текущего года считаем средний чек на платящего игрока: 

select date_format(event_timestamp,'%M') as Month,
avg(amount) ARPPU  
from table_1 
where year(event_timestamp)=year(now()) and amount is not null
GROUP BY 1
order by 1;
