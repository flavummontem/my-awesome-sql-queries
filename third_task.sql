-- По каждой неделе текущего года считаем количество активных пользователей: 
-- Активных считаем, как совершивших любое действие

select date_format(event_timestamp,'%w'),count(distinct user_id) Number_of_active_users  
from table_1 
where year(event_timestamp)=year(now())
GROUP BY 1;
