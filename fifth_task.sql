-- По каждому дню последних двух недель считаем процент пользователей, воспользовавшихся приложением на 3-й и 7-й день, считая со дня своей регистрации: 
-- Сначала считаю процент от всех зарегистрированных в этот день, т.е берем всех кто зарегался в этот день и заходил в течение 3/7 дней и делим на общее кол-во зарегистрированных.

select date(a.dt_registr), count(distinct t1.user_id)/count(distinct b.user_id)*100 as prc_day_3_retention , count(distinct t2.user_id)/count(distinct b.user_id)*100 as prc_day_7_retention
from
(select user_id, min(date(event_timestamp)) as dt_registr 
from table_1 
GROUP BY 1) as a
left join table_1 t1 on t1.user_id = a.user_id   and datediff(t1.event_timestamp,a.dt_registr) = 4
left join table_1 t2 on t2.user_id = a.user_id  and datediff(t2.event_timestamp,dt_registr) = 5
left join (select user_id, min(date(event_timestamp)) as dt_registr 
from table_1 
GROUP BY 1) as b on a.dt_registr= b.dt_registr
where dt_registr> date(now())- interval '14' day
group by 1;
