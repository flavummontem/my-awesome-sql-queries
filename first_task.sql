-- По каждому дню последних двух недель считаем количество новых пользователей: 
-- Сначала выбрала минимальную дату события, считаем ее за дату регистрации, предполагается, что нельзя зарегистрироваться после проведения платежа, если это не так, нужно поставить доп ограничение where amount is null

select dt_registr, count(user_id) as number_of_new_users 
from ( 
	select user_id, min(date(event_timestamp)) as dt_registr 
    from table_1 
    GROUP BY 1) as a
where dt_registr> date(now())- interval '14' day;
